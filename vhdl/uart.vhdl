

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.my_lib.all;

entity uart is
    generic(
        clk_freq    : integer := 50000000;
        word_width  : integer := 8;
        parity      : std_logic := '0';
        parity_even : std_logic := '0';
        stop_bits   : integer := 1
    );
    port (
        clk         : in    std_logic;

        -- Baud rate divisor
        clk_target  : in    unsigned(31 downto 0) := (others => '0');

        -- Transmit data and handshake
        tx_data     : in    std_logic_vector(word_width-1 downto 0);
        tx_idle     : out   std_logic := '1';
        tx_start    : in    std_logic := '0';

        -- Receive data and handshake
        rx_data     : out   std_logic_vector(word_width-1 downto 0);
        rx_new_word : out   std_logic := '0';

        -- UART Outputs
        tx          : out   std_logic := '1';
        rx          : in    std_logic
    );
end entity;

architecture behavioural of uart is 

    type tx_state_t is (tx_idle_s, tx_start_s, tx_data_s, tx_parity_s, tx_stop_s);
    type rx_state_t is (rx_idle_s, rx_start_s, rx_data_s, rx_parity_s, rx_stop_s);

    signal tx_state                 : tx_state_t := tx_idle_s;
    signal rx_state                 : rx_state_t := rx_idle_s;

    signal rx_clk_target_sig  : unsigned(31 downto 0);
    signal rx_count_sig       : unsigned(31 downto 0)
                                    := (others => '0');
    signal rx_bit_count_sig   : unsigned(f_log2(word_width +1) downto 0)
                                    := (others => '0');
    signal rx_word_sig        : std_logic_vector(word_width-1 downto 0)
                                    := (others => '0');
    signal rx_stop_count_sig  : unsigned(1 downto 0) := (others => '0');
    signal rx_parity_bit_sig  : std_logic := '0';

    signal rx_parity_correct_sig : std_logic := '0';

begin

    transmit : process (clk)
        variable tx_count           : unsigned(23 downto 0) 
                                        := (others => '0');
        variable tx_bit_count       : unsigned(f_log2(word_width) downto 0) 
                                        := (others => '0');
        variable tx_word            : std_logic_vector(word_width-1 downto 0)
                                        := (others => '0');
        variable parity_bit         : std_logic := '0';
        variable tx_stop_count      : unsigned(1 downto 0) := (others => '0');
    begin
        if (rising_edge(clk)) then
            tx <= '1';
            tx_idle <= '0';
            case (tx_state) is
                when tx_idle_s =>
                    tx_idle <= '1';
                    if (tx_start = '1') then
                        tx_word := tx_data;
                        tx_idle <= '0';
                        tx_state <= tx_start_s;
                    end if;
                when tx_start_s =>
                    tx <= '0';
                    if (tx_count < 2*clk_target+1) then
                        tx_count := tx_count + 1;
                    else
                        tx_count := X"000000";
                        tx_state <= tx_data_s;
                        tx_bit_count := (others => '0');
                        if (parity = '1') then
                            if (parity_even = '1') then
                                parity_bit := '0';
                            else
                                parity_bit := '1';
                            end if;
                        end if;
                    end if;
                when tx_data_s =>
                    tx <= tx_word(to_integer(tx_bit_count));
                    if (tx_count < 2 * clk_target + 1) then
                        tx_count := tx_count + 1;
                    else
                        tx_count := X"000000";

                        if (parity = '1') then
                            parity_bit := parity_bit xor tx_word(to_integer(tx_bit_count));
                        end if;

                        if (tx_bit_count < word_width - 1) then
                            tx_bit_count := tx_bit_count + 1;
                        else
                            tx_bit_count := (others => '0');
                            if (parity = '1') then
                                tx_state <= tx_parity_s;
                            else
                                tx_state <= tx_stop_s;
                            end if;
                        end if;
                    end if;
                when tx_parity_s =>
                    tx <= parity_bit;
                    if (tx_count < 2*clk_target+1) then
                        tx_count := tx_count + 1;
                    else 
                        tx_count := X"000000";

                        tx_state <= tx_stop_s;
                    end if;
                when tx_stop_s =>
                    tx <= '1';
                    if (tx_count < 2*clk_target+1) then
                        tx_count := tx_count + 1;
                    else
                        tx_count := X"000000";

                        if (tx_stop_count < stop_bits - 1) then
                            tx_stop_count := tx_stop_count + 1;
                        else 
                            tx_state <= tx_idle_s;
                            tx_stop_count := (others => '0');
                        end if;
                    end if;
            end case; -- tx_state
        end if;
    end process;

    receive : process (clk)
        variable rx_clk_target      : unsigned(31 downto 0);
        variable rx_count           : unsigned(31 downto 0)
                                        := (others => '0');
        variable rx_bit_count       : unsigned(f_log2(word_width +1) downto 0)
                                        := (others => '0');
        variable rx_word            : std_logic_vector(word_width-1 downto 0)
                                        := (others => '0');
        variable parity_bit         : std_logic := '0';
        variable rx_stop_count      : unsigned(1 downto 0) := (others => '0');
        variable parity_correct     : std_logic := '0';
    begin
        if (rising_edge(clk)) then
            rx_clk_target_sig <= rx_clk_target;
            rx_count_sig <= rx_count;
            rx_bit_count_sig <= rx_bit_count;
            rx_word_sig <= rx_word;
            rx_stop_count_sig <= rx_stop_count;
            rx_parity_bit_sig <= parity_bit;
            rx_parity_correct_sig <= parity_correct;
            rx_new_word <= '0';

            case (rx_state) is
                when rx_idle_s =>
                    rx_clk_target := resize(((2 * clk_target + 1) / 16), 32);
                    if (rx_count < rx_clk_target) then
                        rx_count := rx_count + 1;
                    else
                        rx_count := X"00000000";
                        if (rx = '0') then
                            rx_state <= rx_start_s;

                            if (parity = '1') then
                                if (parity_even = '1') then
                                    parity_bit := '0';
                                else
                                    parity_bit := '1';
                                end if;
                            end if;
                        end if;
                    end if;
                when rx_start_s =>
                    if (rx_count < rx_clk_target) then
                        rx_count := rx_count + 1;
                    else
                        rx_count := X"00000000";
                        if (rx = '0') then
                            if (rx_bit_count < 7) then 
                                rx_bit_count := rx_bit_count + 1;
                            else
                                -- In the middle of the bit. This is when we 
                                -- should sample for all other bit periods
                                rx_bit_count := (others => '0');
                                rx_state <= rx_data_s;
                            end if;
                        else
                            rx_bit_count := (others => '0');
                            rx_state <= rx_idle_s;
                        end if;
                    end if;
                when rx_data_s =>
                    if (rx_count < 2 * clk_target + 1) then
                        rx_count := rx_count + 1;
                    else
                        rx_count := X"00000000";
                        rx_word(to_integer(rx_bit_count)) := rx;
                        if (parity = '1') then
                            parity_bit := parity_bit xor rx_word(to_integer(rx_bit_count));
                        end if;
                        rx_bit_count := rx_bit_count + 1;
                        if (rx_bit_count > 7) then
                            rx_bit_count := (others => '0');
                            if (parity = '1') then
                                rx_state <= rx_parity_s;
                            else
                                rx_state <= rx_stop_s;
                            end if;
                        end if;
                    end if;
                when rx_parity_s =>
                    if (rx_count < 2 * clk_target + 1) then
                        rx_count := rx_count + 1;
                    else
                        rx_count := X"00000000";
                        if (parity_bit = rx) then
                            parity_correct := '1';
                        else
                            parity_correct := '0';
                        end if;
                        rx_state <= rx_stop_s;
                    end if;
                when rx_stop_s =>
                    if (rx_count < 2 * clk_target + 1) then
                        rx_count := rx_count + 1;
                    else
                        rx_count := X"00000000";

                        if (rx_stop_count < stop_bits - 1) then
                            rx_stop_count := rx_stop_count + 1;
                        else 
                            rx_state <= rx_idle_s;
                            rx_stop_count := (others => '0');
                            rx_new_word <= '1';
                            rx_data <= rx_word;
                        end if;
                    end if;
            end case;
        end if;
    end process;

end architecture;