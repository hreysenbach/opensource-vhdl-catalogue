set DUT     counter

array set dependencies {
    register
    fifo 
}

set DUT_TB  ${DUT}_tb
set ROOTDIR ../..
set SRC_DIR $ROOTDIR/verilog
set SIM_DIR $ROOTDIR/simulations/$DUT_TB

if {[file exists rtl_work]} {
    vdel -lib rtl_work -all
}

vlib        rtl_work
vmap work   rtl_work

foreach {dependency} [array get dependencies] {
    vlog -work work +define+SIMULATION      $SRC_DIR/$dependency.v
}

vlog -work work     +define+SIMULATION      $SRC_DIR/$DUT.v
vlog -work work -sv +define+SIMULATION      $SIM_DIR/$DUT_TB.sv

vsim -t 1ps -fsmdebug -L work -voptargs="+acc" $DUT_TB

config wave -namecolwidth 200 -valuecolwidth 100

add wave -group "Clocks and Reset"  -radix unsigned     /${DUT_TB}/ticks
add wave -group "Clocks and Reset"                      /${DUT_TB}/clk
add wave -group "Clocks and Reset"                      /${DUT_TB}/rst

add wave -group "Enables"                               /${DUT_TB}/en

add wave -group "Data"              -hex                /${DUT_TB}/out

run -all
