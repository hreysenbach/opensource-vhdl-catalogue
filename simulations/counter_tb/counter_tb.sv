`timescale 1ns/1ps

module counter_tb ();

`define     NS_PER_US       1000
`define     US_PER_MS       1000
`define     MS_PER_S        1000
`define     NS_PER_MS       `NS_PER_US * `US_PER_MS
`define     US_PER_S        `US_PER_MS * `MS_PER_S
`define     NS_PER_S        `NS_PER_MS * `MS_PER_S


localparam  CLK_PERIOD_NS   = 20;

localparam  data_width      = 32;

reg                         clk;
reg                         rst;
reg                         en;
wire    [data_width-1:0]    out;

reg     [31:0]              ticks;

reg     [data_width-1:0]    check_counter;

initial begin
    clk     <= 1'b0;
    rst     <= 1'b1;
    ticks   <= {32{1'b0}};
end

initial begin
    en      <= 1'b0;
end

always 
begin
    #(CLK_PERIOD_NS/2) 
    clk = !clk;
end

always @(posedge clk)
begin
    ticks <= ticks + 1'b1;
end

// rst
always @(posedge clk)
begin
    case (ticks)
        10:
            rst <= 1'b0;
        18:
            rst <= 1'b1;
        19:
            rst <= 1'b0;
        1024:
            $finish;
        default:
        begin
        end
    endcase
end

always @(posedge clk)
begin
    if (ticks == 12) begin
        en <= 1'b1;
    end else if (ticks == 14) begin
        en <= 1'b0;
    end else if (ticks == 16) begin
    end else if (ticks == 20) begin
    end else if (ticks > 128 && ticks < 512) begin
        en <= 1'b1;
    end
end

always @(posedge clk)
begin
    if(rst == 1'b1) begin
        check_counter <= {data_width{1'b0}};
    end else begin
        if (en == 1'b1) begin
            check_counter <= check_counter + 1;
        end
    end
end

always @(posedge clk)
begin
    if (check_counter != out) begin
        $error("%0d: Counter does not match functionality, checker = %H, actual = %H", 
            ticks, 
            check_counter,
            out);
    end
end


counter
#(
    .data_width         (data_width)
) inst_dut (
    .clk                (clk),
    .rst                (rst),
    .en                 (en),
    .out                (out)
);


endmodule
