module fifo (
    clk,
    rst,
    wren,
    rden,
    d,
    q,
    empty,
    full,
    underflow,
    overflow,
    num_words
);

parameter data_width        = 32;
parameter number_of_words   = 256;

localparam addr_size        = $clog2(number_of_words);

input   wire                        clk;
input   wire                        rst;
input   wire                        wren;
input   wire                        rden;
input   wire    [data_width-1:0]    d;
output  wire    [data_width-1:0]    q;
output  wire                        empty;
output  wire                        full;
output  wire                        underflow;
output  wire                        overflow;
output  wire    [addr_size-1:0]     num_words;

wire                                s_read_while_write;
wire                                s_underflow;
reg                                 r_underflow;
wire                                s_overflow;
reg                                 r_overflow;

reg             [addr_size-1:0]     r_num_words;
wire            [addr_size-1:0]     r_write_ptr;
wire            [addr_size-1:0]     r_read_ptr;
wire                                s_read_ptr_incr;
wire                                s_write_ptr_incr;

ram_dual_port 
#(
    .data_width                     (data_width),
    .ram_size                       (number_of_words)
) inst_fifo_ram (
    .clk                            (clk),
    .rst                            (rst),
    .wren                           (wren),
    .rden                           (rden),
    .wr_addr                        (r_write_ptr),
    .rd_addr                        (r_read_ptr),
    .data                           (d),
    .q                              (q)
);

register
#(
    .data_width                     (addr_size)
) read_ptr_cntr (
    .clk                            (clk),
    .rst                            (rst),
    .en                             (s_read_ptr_incr),
    .d                              (r_read_ptr + 1'b1),
    .q                              (r_read_ptr)
);

register
#(
    .data_width                     (addr_size)
) write_ptr_cntr (
    .clk                            (clk),
    .rst                            (rst),
    .en                             (s_write_ptr_incr),
    .d                              (r_write_ptr + 1'b1),
    .q                              (r_write_ptr)
);

// Status flags
assign num_words    = r_num_words;
assign overflow     = r_overflow;
assign underflow    = r_underflow;
assign empty        = (r_num_words == {addr_size{1'b0}}) 
                        ? 1'b1 : 1'b0;

assign full         = ( (r_num_words == {addr_size{1'b1}}) 
                        & wren 
                        & !s_read_while_write) 
                        ? 1'b1 : 1'b0;


assign s_read_while_write = rden & wren;

assign s_underflow = empty & rden & !s_read_while_write;
assign s_overflow  = full  & wren & !s_read_while_write;

assign s_read_ptr_incr  = (!rst & rden & !empty);
assign s_write_ptr_incr = (!rst & wren & !full);

always @(posedge clk)
begin
    if (rst == 1'b1) begin
        r_num_words <= {addr_size{1'b0}};
    end else begin
        if (!s_read_while_write) begin
            if ((wren == 1'b1) & !full) begin
                r_num_words <= r_num_words + 1'b1;
            end else if ((rden == 1'b1) & !empty) begin
                r_num_words <= r_num_words - 1'b1;
            end
        end
    end
end

always @(posedge clk)
begin
    if (rst == 1'b1) begin
        r_underflow <= 1'b0;
        r_overflow  <= 1'b0;
    end else begin
        if (s_underflow == 1'b1) begin
            r_underflow <= 1'b1;
        end

        if (s_overflow == 1'b1) begin
            r_overflow <= 1'b1;
        end
    end
end

endmodule
