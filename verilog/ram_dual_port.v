module ram_dual_port (
    clk,
    rst,
    wren,
    rden,
    wr_addr,
    rd_addr,
    data,
    q
);

parameter data_width                = 32;
parameter ram_size                  = 256;

localparam addr_size                = $clog2(ram_size);

input   wire                        clk;
input   wire                        rst;
input   wire                        wren;
input   wire                        rden;
input   wire    [addr_size-1:0]     wr_addr;
input   wire    [addr_size-1:0]     rd_addr;
input   wire    [data_width-1:0]    data;
output  wire    [data_width-1:0]    q;

reg             [data_width-1:0]    output_data;
reg             [data_width-1:0]    ram_array       [ram_size-1:0];
wire                                write_enable;

`ifdef SIMULATION
    integer                         i;
    initial begin
        for (i=0; i < ram_size; i=i+1) begin
            ram_array[i] <= {data_width{1'b0}};
        end
    end
`endif

assign write_enable = wren & !rst;
assign q = rst == 1'b0 ? output_data : {data_width{1'b0}};

always @(posedge clk)
begin
    if (write_enable == 1'b1) begin
        ram_array[wr_addr] <= data;
    end

    if (rst == 1'b1) begin
        output_data <= {data_width{1'b0}};
    end else if (rden == 1'b1) begin
        output_data <= ram_array[rd_addr];
    end
end

endmodule
